<?php

use helpers\Languages;
use helpers\Cookies;


class LanguageSetter
{
    public $supportedLanguages = ['en-US', 'pl-PL', 'de-DE','fr-FR'];
    public $forceLanguage = null;
    public $langFromBrowser = null;
    
    

    public function bootstrap($app)
    {
        //preferredLanguage:  aa-AA
        //user->language aa-AA
        //app->language: aa-AA
        
        if($this->isForceLanguage()){
            $preferredLanguage = $this->forceLanguage;
        }else{
            // Find preferred language in cookie, app settings and user settings
            $this->langFromBrowser = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'],0,2);
            
            $isGuest = $app->user->isGuest;
            $user = $app->user->identity;
            
            //get from cookie
            $preferredLanguage = $this->getCookieLang();
            //not lang in cookies
            if(!$preferredLanguage){
                $preferredLanguage = $this->getBrowserLang();
            }
            //not lang in browser
            if(!$preferredLanguage){
                $preferredLanguage = $app->request->getPreferredLanguage($this->supportedLanguages);
            }
            
            //Authorized user
            if(!$isGuest){
                if(!empty($user->language)){
                    $preferredLanguage = $user->language;
                }else{
                    $user->language = $preferredLanguage;
                    $user->save();
                }
            }
        }

        $app->language = $preferredLanguage;
        Cookies::setCookie('language', $preferredLanguage);
        $preferredLanguage = str_replace('-', '_', $preferredLanguage);
        setlocale(LC_ALL, $preferredLanguage . '.utf8');

        return $preferredLanguage;
    }


    private function getBrowserLang(){
        return Languages::getLangCodeFromShortCode($this->langFromBrowser);
    }

    private function getCookieLang(){
        $cookieLanguage = Cookies::getCookie('language');
        if(!empty($cookieLanguage)){
            return $cookieLanguage->value;
        }
        return false;
    }

    private function checkLanguageisNumeric($preferredLanguage){
        // Set preferred language
        if( is_numeric( $preferredLanguage ) ){
            $preferredLanguage = Languages::getLangCode( $preferredLanguage );
        }

        return $preferredLanguage;
    }

    private function isForceLanguage(){
        if( $this->forceLanguage ) {
            return $this->forceLanguage;
        }
        return false;
    }

}