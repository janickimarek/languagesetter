<?php

class App
{

    public $language = null;
    public $user = null;
    public $request = null;

    function __construct(){
        $this->user = new UserAuth(); 
        $this->request = new Request();     
    }    
}