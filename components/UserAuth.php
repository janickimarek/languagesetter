<?php

class UserAuth
{

    public $isGuest = true;
    public $identity = null;
    
    public function authorize(){
        $this->isGuest = false;
        $this->identity = new User();
    }
}