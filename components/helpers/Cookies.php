<?php
namespace helpers;

class Cookies
{

    static function getCookie($name){
        if(isset($_COOKIE[$name])){
            return (object) [
                'value' => $_COOKIE[$name]
            ];
        }
        return null;
    }

    static function setCookie($name, $value){
        return setcookie($name, $value);
    }
}