<?php 
class LanguageSetterTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    
    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testLanguageSetterForGuest()
    {
        require_once('components/LanguageSetter.php');
        require_once('components/UserAuth.php');
        require_once('components/User.php');
        require_once('components/App.php');
        require_once('components/Request.php');
        require_once('components/helpers/Cookies.php');
        require_once('components/helpers/Languages.php');
        
        $_SERVER['HTTP_ACCEPT_LANGUAGE'] = "en";
        $app = new App();
        $app->user->authorize();
        $LanguageSetter = new LanguageSetter();
        $lang = $LanguageSetter->bootstrap($app);
    }

    public function testLanguageSetterForAuthorizedUser()
    {
        require_once('components/LanguageSetter.php');
        require_once('components/UserAuth.php');
        require_once('components/User.php');
        require_once('components/App.php');
        require_once('components/Request.php');
        require_once('components/helpers/Cookies.php');
        require_once('components/helpers/Languages.php');
        
        $_SERVER['HTTP_ACCEPT_LANGUAGE'] = "en";
        $app = new App();
        $LanguageSetter = new LanguageSetter();
        $lang = $LanguageSetter->bootstrap($app);
    }
    
}